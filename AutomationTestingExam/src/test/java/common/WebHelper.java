package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class WebHelper {

    WebDriver driver;
    WebElement element;

    public WebHelper(WebDriver driver){
         this.driver = driver;
    }

    public void implicitlyWait(long timeOut){
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
    }

    public void goToUrl(String url) {
        driver.get(url);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void refreshPage() {
        driver.navigate().refresh();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void goBack() {
        driver.navigate().back();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void openMaximizeWindow() {
        driver.manage().window().maximize();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickElement(String xpath) {
        element = driver.findElement(By.xpath(xpath));
        element.click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickButton(String btnName) {
        String[] button = {"//button[text()='", "']"};
        element = driver.findElement(By.xpath(button[0] + btnName + button[1]));
        element.click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickSpanTagWithText(String text) {
        String[] span = {"//span[text()='", "']"};
        element = driver.findElement(By.xpath(span[0] + text + span[1]));
        element.click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickLinkTagWithText(String text) {
        String[] linkTag = {"//a[text()='", "']"};
        element = driver.findElement(By.xpath(linkTag[0] + text + linkTag[1]));
        element.click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickLabelTagWithText(String text) {
        String[] labelTag = {"//label[text()='", "']"};
        clickElement(labelTag[0]+ text + labelTag[1]);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void inputText(String xpath, String value) {
        element = driver.findElement(By.xpath(xpath));
        element.sendKeys(value);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clearText(String xpath) {
        element = driver.findElement(By.xpath(xpath));
        element.clear();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void elementShouldBe(String xpath, String expectedResult) {
        element = driver.findElement(By.xpath(xpath));
        String actualResult = element.getText();

        Assert.assertEquals(actualResult, expectedResult);


    }


}
