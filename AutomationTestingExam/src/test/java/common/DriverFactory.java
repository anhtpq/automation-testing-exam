package common;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static configurations.Common.baseUrl;

public class DriverFactory {

    static WebDriver driver;

    public DriverFactory(String type) throws Exception {
        driver = createDriver(type);
    }

    public WebDriver createDriver(String type) throws Exception {
        switch (type)
        {
            case "Chrome":
//                WebDriverManager.chromedriver().setup();
//                driver = new ChromeDriver();
                System.setProperty("webdriver.chrome.driver","/Users/anhtran/IdeaProjects/AutomationTestingExam/src/test/java/test/scripts/chromedriver");
                driver = new ChromeDriver();
                break;
            case "FireFox":
                driver = new FirefoxDriver();
                break;
            default: throw new Exception("Please enter correct driver");
        }

        return driver;
    }

    public void  closeDriver(){
        driver.close();
        driver.quit();

    }
}
