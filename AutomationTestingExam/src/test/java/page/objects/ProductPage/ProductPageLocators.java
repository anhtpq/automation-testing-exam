package page.objects.ProductPage;

public class ProductPageLocators {


    //Xpath
    //Product screen
    public static String btnCreate = "//button[@class='btn btn-primary o-kanban-button-new']";
    public static String btnSave = "//button[@class='btn btn-primary o_form_button_save']";
    public static String btnDiscard = "//button[@class='btn btn-secondary o_form_button_cancel']";
    public static String txtProductName = "//input[@name='name']";
    public static String txtInternalReference = "//input[@name='default_code']";
    public static String txtBarcode = "//input[@name='barcode']";

    //Update Quantity screen
    public static String btnCreateUpdateQuantity = "//button[@class='btn btn-primary o_list_button_add']";
    public static String btnSaveUpdateQuantity = "//button[@class='btn btn-primary o_list_button_save']";
    public static String txtCountedQuantity = "//input[@name='inventory_quantity']";

    //Text
    public static String lbUpdateQuantity = "Update Quantity";
}
