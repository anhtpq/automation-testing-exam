package page.objects.ProductPage;

import common.WebHelper;
import org.openqa.selenium.WebDriver;

import static page.objects.ProductPage.ProductPageLocators.*;

public class ProductPageFunctions {

    WebDriver driver;
    WebHelper webHelper;

    public ProductPageFunctions(WebDriver driver) {
        this.driver = driver;

        webHelper = new WebHelper(driver);
    }

    public void clickOnCreateBtn() {
        webHelper.clickElement(btnCreate);
    }

    public void inputProductName(String productName) {
        webHelper.inputText(txtProductName, productName);
    }

    public void clickOnSaveBtn() {
        webHelper.clickElement(btnSave);
    }

    public void clickOnUpdateQuantity() {
        webHelper.clickSpanTagWithText(lbUpdateQuantity);
    }

    public void inputCountedQuantity(String countedQuantity) {
        webHelper.clearText(txtCountedQuantity);
        webHelper.inputText(txtCountedQuantity, countedQuantity);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickOnCreateUpdateQuantity() {
        webHelper.clickElement(btnCreateUpdateQuantity);
    }

    public void clickOnSaveUpdateQuantityBtn() {
        webHelper.clickElement(btnSaveUpdateQuantity);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void createNewProduct(String productName) throws InterruptedException {
        clickOnCreateBtn();
        inputProductName(productName);
        clickOnSaveBtn();
        Thread.sleep(1000);
    }

    public void updateQuantity(String countedQuantity) {
        clickOnUpdateQuantity();
        clickOnCreateUpdateQuantity();
        inputCountedQuantity(countedQuantity);
        clickOnSaveUpdateQuantityBtn();
    }
}
