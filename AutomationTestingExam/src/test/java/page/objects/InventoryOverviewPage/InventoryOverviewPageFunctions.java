package page.objects.InventoryOverviewPage;

import common.WebHelper;
import org.openqa.selenium.WebDriver;

public class InventoryOverviewPageFunctions {

    WebDriver driver;
    WebHelper webHelper;

    public InventoryOverviewPageFunctions(WebDriver driver) {
        this.driver = driver;

        webHelper = new WebHelper(driver);
    }

    public void clickOnMenuItem(String menuItemName){
        webHelper.clickSpanTagWithText(menuItemName);
    }

    public void clickOnSubMenuItem(String subMenuItem){
        webHelper.clickLinkTagWithText(subMenuItem);
    }

    public void goToProductsPage() {
        clickOnMenuItem(InventoryOverviewPageLocators.itemProducts);
        clickOnSubMenuItem(InventoryOverviewPageLocators.subItemProducts);
    }
}
