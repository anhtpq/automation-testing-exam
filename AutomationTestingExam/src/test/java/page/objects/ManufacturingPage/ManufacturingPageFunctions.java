package page.objects.ManufacturingPage;

import common.WebHelper;
import org.openqa.selenium.WebDriver;

import static page.objects.ManufacturingPage.ManufacturingPageLocators.*;

public class ManufacturingPageFunctions {

    WebDriver driver;
    WebHelper webHelper;

    public ManufacturingPageFunctions(WebDriver driver) {
        this.driver = driver;

        webHelper = new WebHelper(driver);
    }

    public void clickOnCreateBtn() {
        webHelper.clickElement(btnCreate);
    }

    public void clickOnConfirmBtn() {
        webHelper.clickSpanTagWithText(btnConfirm);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickOnMarkAsDoneBtn() {
        webHelper.clickElement(btnMarkAsDone);
    }

    public void inputProductName(String productName) {
        webHelper.inputText(txtProduct, productName);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        webHelper.clickLabelTagWithText(lbProduct);
    }

    public void clickOnOkBtn() {
        webHelper.clickSpanTagWithText("Ok");
    }

    public void clickOnApplyBtn() {
        webHelper.clickSpanTagWithText("Apply");
    }

    public void verifyProductInformation(String productName) {
        webHelper.elementShouldBe(lbProductName[0] + productName + lbProductName[1], productName);
    }

}
