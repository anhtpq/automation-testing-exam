package page.objects.ManufacturingPage;

public class ManufacturingPageLocators {

    //Xpath
    public static String btnCreate = "//button[@class='btn btn-primary o_list_button_add']";
    public static String btnDone = "//button[contains(text(), 'Done')]";
    public static String txtProduct = "//label[text()='Product']/ancestor::tr//input[@class='o_input ui-autocomplete-input']";
    public static String btnMarkAsDone = "//div[@class='o_statusbar_buttons']/button[4]/span";


    //Text
    public static String lbProduct = "Product";
    public static String btnConfirm = "Confirm";

    //Dynamic Xpath
    //Product name
    public static String[] lbProductName = {"//span[text()='", "']"};

}
