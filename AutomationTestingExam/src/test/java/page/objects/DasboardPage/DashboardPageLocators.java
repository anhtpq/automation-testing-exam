package page.objects.DasboardPage;

public class DashboardPageLocators {

    public static String iconApplication = "//a[@title='Home menu']";
    public static String lbInventory = "//*[text()='Inventory']";
    public static String lbManufacturing = "//*[text()='Manufacturing']";

}
