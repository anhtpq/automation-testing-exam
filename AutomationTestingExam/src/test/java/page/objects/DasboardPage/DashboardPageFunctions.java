package page.objects.DasboardPage;

import common.WebHelper;
import org.openqa.selenium.WebDriver;

import static page.objects.DasboardPage.DashboardPageLocators.*;

public class DashboardPageFunctions {

    WebDriver driver;
    WebHelper webHelper;

    public DashboardPageFunctions(WebDriver driver) {
        this.driver = driver;

        //Call all method on other class
        webHelper = new WebHelper(driver);
    }

    public void clickOnInventory() {
        webHelper.clickElement(lbInventory);
    }

    public void clickOnApplicationIcon() {
        webHelper.clickElement(iconApplication);
    }

    public void clickOnManufacturing () {
        webHelper.clickElement(lbManufacturing);
    }
}
