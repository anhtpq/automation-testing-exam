package page.objects.LoginPage;

public class LoginPageLocators {

    //Xpath on pages
    public static String[] txtInputLoginFields = {"//*[text()='", "']//ancestor::div/input"};

    //Label name on page
    public static String lbUsername = "Email";
    public static String lbPassword = "Password";
    public static String lbLoginBtn = "Log in";
}
