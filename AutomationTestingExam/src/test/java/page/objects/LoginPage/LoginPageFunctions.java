package page.objects.LoginPage;

import common.WebHelper;
import org.openqa.selenium.WebDriver;

import static page.objects.LoginPage.LoginPageLocators.*;

public class LoginPageFunctions {

    WebDriver driver;
    WebHelper webHelper;

    public LoginPageFunctions(WebDriver driver) {
        this.driver = driver;

        webHelper = new WebHelper(driver);
    }

    public void inputValueIntoLoginField(String fieldName, String value){
        webHelper.inputText(txtInputLoginFields[0] +fieldName + txtInputLoginFields[1], value);
    }

    public void clickOnLoginBtn(){
        webHelper.clickButton(lbLoginBtn);
    }

    public void loginToSystem(String username, String password){
        inputValueIntoLoginField(lbUsername, username);
        inputValueIntoLoginField(lbPassword, password);
        clickOnLoginBtn();
    }
}
