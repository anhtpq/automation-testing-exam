package test.scripts;

import common.DriverFactory;
import common.WebHelper;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.devtools.v85.log.Log;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import page.objects.DasboardPage.DashboardPageFunctions;
import page.objects.InventoryOverviewPage.InventoryOverviewPageFunctions;
import page.objects.LoginPage.LoginPageFunctions;
import page.objects.ManufacturingPage.ManufacturingPageFunctions;
import page.objects.ProductPage.ProductPageFunctions;

import java.util.logging.Logger;

import static configurations.Common.*;
import static data.TestCasesData.countedQuantity;
import static data.TestCasesData.productName;

public class TestCases {


    WebDriver driver;
    WebHelper webHelper;
    LoginPageFunctions loginPageFunctions;
    DashboardPageFunctions dashboardPageFunctions;
    InventoryOverviewPageFunctions inventoryOverviewPageFunctions;
    ProductPageFunctions productPageFunctions;
    ManufacturingPageFunctions manufacturingPageFunctions;

    private static Logger Log = Logger.getLogger(org.openqa.selenium.devtools.v85.log.Log.class.getName());

    @BeforeTest
    public void setup() throws Exception {
        switch (browserName)
        {
            case "Chrome":
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            case "FireFox":
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            default: throw new Exception("Please enter correct driver");
        }

        webHelper = new WebHelper(driver);

        webHelper.implicitlyWait(timeOut);

        Log.info("Go to website");
        webHelper.goToUrl(baseUrl);

        Log.info("Open Maximize Window");
        webHelper.openMaximizeWindow();
    }

    @AfterTest
    public void tearDown(){
        driver.close();
        driver.quit();
    }

    @Test
    public void testCase() throws InterruptedException {
        loginPageFunctions = new LoginPageFunctions(driver);
        dashboardPageFunctions = new DashboardPageFunctions(driver);
        inventoryOverviewPageFunctions = new InventoryOverviewPageFunctions(driver);
        productPageFunctions = new ProductPageFunctions(driver);
        manufacturingPageFunctions = new ManufacturingPageFunctions(driver);

        Log.info("Login successfully");
        loginPageFunctions.loginToSystem(username, password);

        Log.info("Click on Inventory");
        dashboardPageFunctions.clickOnInventory();

        Log.info("Go to product page");
        inventoryOverviewPageFunctions.goToProductsPage();

        Log.info("Create product and update Quantity");
        productPageFunctions.createNewProduct(productName);
        productPageFunctions.updateQuantity(countedQuantity);

        Log.info("Go to Manufacturing page");
        dashboardPageFunctions.clickOnApplicationIcon();
        dashboardPageFunctions.clickOnManufacturing();

        Log.info("Create a new order");
        manufacturingPageFunctions.clickOnCreateBtn();
        manufacturingPageFunctions.inputProductName(productName);
        manufacturingPageFunctions.clickOnConfirmBtn();

        Log.info("Mark as Done of the order");
        manufacturingPageFunctions.clickOnMarkAsDoneBtn();
        manufacturingPageFunctions.clickOnOkBtn();
        manufacturingPageFunctions.clickOnApplyBtn();

        Log.info("Verify information");
        manufacturingPageFunctions.verifyProductInformation(productName);

    }
}
